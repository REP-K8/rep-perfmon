INFLUX_HOST=172.17.0.1

K6_DUR=10s
K6_VUS=1
[ $# -ge 1 ] && K6_DUR=$1
[ $# -ge 2 ] && K6_VUS=$2

# Name resolution works with '--network host' on crispml and influxdb
# DS_INFLUXDB is now hardcoded in the grafana k6 dashboard so needs to be set here (actually was a 'DS_INFLUXDB' variable that i converted to string)
docker run --rm --network host -i loadimpact/k6 run --out influxdb=http://$INFLUX_HOST:8086/DS_INFLUXDB -d $K6_DUR -u $K6_VUS - <sleep.js

