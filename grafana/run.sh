#!/bin/bash

PLUGINS="GF_INSTALL_PLUGINS"
PLUGINS="${PLUGINS}=grafana-clock-panel,grafana-simple-json-datasource"
#PLUGINS="${PLUGINS},4411"
#PLUGINS="${PLUGINS},https://grafana.com/grafana/dashboards/4411"
#PLUGINS="${PLUGINS},https://grafana.com/grafana/dashboards/2587"

# docker run --rm -d -p 3000:3000 -e "$PLUGINS" --name rep-grafana rep-grafana:latest
docker run --rm -d --network host --name rep-grafana rep-grafana:latest

