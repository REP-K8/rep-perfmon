#!/bin/bash

for i in `docker ps | grep -v COMMAND | awk '{print $NF}'`
do 
	echo $i 
	docker exec -it $i /bin/bash -c "ip address | grep inet | grep global";
done

