import http from "k6/http";
import { check, group, sleep } from "k6";
import { Counter, Rate, Trend } from "k6/metrics";

export default function() {
  group ("grpEcho", function() {
    let res0 = http.get("http://172.17.0.3:8000/echo?msg=hello-world!");
    let checkRes0 = check(res0, {"status is 200": (r) => r.status === 200});
    //checkFailureRate.add(!checkRes0);
  });
  group ("grpIngest", function() {
    let res1 = http.get("http://172.17.0.3:8000/crispml/repDataIngest?from=from&to=to");
    let checkRes1 = check(res1, {"status is 200": (r) => r.status === 200});
    //checkFailureRate.add(!checkRes1);
    let res2 = http.get("http://172.17.0.3:8000/crispml/repDataPrepare?from=from&to=to");
    let checkRes2 = check(res2, {"status is 200": (r) => r.status === 200});
    //checkFailureRate.add(!checkRes2);
    let res3 = http.get("http://172.17.0.3:8000/crispml/repDataCurate?from=from&to=to");
    let checkRes3 = check(res3, {"status is 200": (r) => r.status === 200});
    //checkFailureRate.add(!checkRes3);
  });
  group ("grpTrain", function() {
    let res4 = http.get("http://172.17.0.3:8000/crispml/repModelTrain?from=from&to=to&model=model");
    let checkRes4 = check(res4, {"status is 200": (r) => r.status === 200});
    //checkFailureRate.add(!checkRes4);
    let res5 = http.get("http://172.17.0.3:8000/crispml/repModelAssess?from=from&to=to&model=model");
    let checkRes5 = check(res5, {"status is 200": (r) => r.status === 200});
    //checkFailureRate.add(!checkRes5);
  });
};
