#!/bin/bash

# Obsolete. Now implemented as ENV variables in Dockerfile
docker run --rm \
      -e INFLUXDB_DB=REPFLUX -e INFLUXDB_ADMIN_ENABLED=true \
      -e INFLUXDB_ADMIN_USER=admin -e INFLUXDB_ADMIN_PASSWORD=admin \
      -e INFLUXDB_USER=influx -e INFLUXDB_USER_PASSWORD=influx \
      -v $PWD:/var/lib/influxdb \
      influxdb /init-influxdb.sh
docker run --rm influxdb influxd config > influxdb.conf
