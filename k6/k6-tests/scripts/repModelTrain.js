import http from "k6/http";
import { check, group, sleep } from "k6";
import { Counter, Rate, Trend } from "k6/metrics";

export let options = {
  noConnectionReuse: true,
  noVUConnectionReuse: true,
};

export default function() {
  group ("grpTrain", function() {
    let res4 = http.get("https://rep-k8s-cluster.eu-de.containers.appdomain.cloud/crispml/repModelTrain?table=CCF_CUR&from=1&to=6&model=rf&weight=10", { timeout: 180000 });
    let checkRes4 = check(res4, {"status is 200": (r) => r.status === 200});
    //checkFailureRate.add(!checkRes4);
  });
};
