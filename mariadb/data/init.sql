
# Must give additional rights so the table created by following statement can be accessed (WEIRD !!!)
# This statements only works after downgrading from 10.4.12 to the latest 10.3
UPDATE mysql.user SET File_priv='Y' WHERE user='RepMariaDBUser' AND host='%';
FLUSH PRIVILEGES;

CREATE TABLE CCF_RAW
(
  TimeStamp INT,
  X01 DOUBLE,
  X02 DOUBLE,
  X03 DOUBLE,
  X04 DOUBLE,
  X05 DOUBLE,
  X06 DOUBLE,
  X07 DOUBLE,
  X08 DOUBLE,
  X09 DOUBLE,
  X10 DOUBLE,
  X11 DOUBLE,
  X12 DOUBLE,
  X13 DOUBLE,
  X14 DOUBLE,
  X15 DOUBLE,
  X16 DOUBLE,
  X17 DOUBLE,
  X18 DOUBLE,
  X19 DOUBLE,
  X20 DOUBLE,
  X21 DOUBLE,
  X22 DOUBLE,
  X23 DOUBLE,
  X24 DOUBLE,
  X25 DOUBLE,
  X26 DOUBLE,
  X27 DOUBLE,
  X28 DOUBLE,
  Amount DOUBLE,
  Class CHAR(1)
)
engine=connect table_type=CSV file_name='/docker-entrypoint-initdb.d/creditcardfraud.zip'
sep_char=',' qchar='"' header=1 zipped=1 option_list='Entry=creditcard.csv,append=0';

