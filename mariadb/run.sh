#!/bin/bash

PWD=`pwd`
docker run -l debug -p 3306:3306 -e MYSQL_ROOT_PASSWORD_FILE=/run/secrets/MYSQL_ROOT_PASSWORD -e MYSQL_DATABASE_FILE=/run/secrets/MYSQL_DATABASE -e MYSQL_USER_FILE=/run/secrets/MYSQL_USER -e MYSQL_PASSWORD_FILE=/run/secrets/MYSQL_PASSWORD  --name rep-mariadb -d rep-mariadb:latest
#docker run -l debug -p 3306:3306 -v $PWD/mysqldb:/var/lib/mysql -e MYSQL_ROOT_PASSWORD_FILE=/run/secrets/MYSQL_ROOT_PASSWORD -e MYSQL_DATABASE_FILE=/run/secrets/MYSQL_DATABASE -e MYSQL_USER_FILE=/run/secrets/MYSQL_USER -e MYSQL_PASSWORD_FILE=/run/secrets/MYSQL_PASSWORD  --name rep-mariadb -d rep-mariadb:latest

