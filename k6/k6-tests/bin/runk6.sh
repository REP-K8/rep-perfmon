#
echo "Executing $0"

# where is the k6 executable ?
echo "Searching for k6 executable"
find / -name k6 -type f -ls

# Statics
K6=k6
SCRIPT_DIR=/k6-tests/scripts
INFLUX_SERVICE=rep-influxdb-service
INFLUX_PORT=8086
INFLUX_DB=DS_INFLUXFB

# Parameters
DUR=60s
VUS=1
SCRIPT=sleep.js

# Parameters
[ $# -ge 1 ] && DUR=$1
[ $# -ge 2 ] && VUS=$2
[ $# -ge 3 ] && SCRIPT=$3

# Generate some output
env | sort

# K6 loadtest parameters via env variables
#if [ ! -z "$K6_DUR" ] && DUR=$K6_DUR
#if [ ! -z "$K6_VUS" ] && VUS=$K6_VUS
#if [ ! -z "$K6_SCRIPT_DIR" ] && SCRIPT_DIR=$K6_SCRIPT_DIR
#if [ ! -z "$K6_SCRIPT" ] && SCRIPT=$K6_SCRIPT

# InfluxDB settings via env variables
#if [ ! -z "$K6_INFLUX_SERVICE" ] && INFLUX_SERVICE=$K6_INFLUX_SERVICE
#if [ ! -z "$K6_INFLUX_PORT" ] && INFLUX_PORT=$K6_INFLUX_PORT
#if [ ! -z "$K6_INFLUX_DB" ] && INFLUX_DB=$K6_INFLUX_DB

CMD="$K6 --out influxdb=http://$INFLUX_SERVICE:$INFLUX_PORT/$INFLUX_DB -d $DUR -u $VUS $SCRIPT_DIR/$SCRIPT"
echo "Running $CMD"
#$CMD

echo "Exiting $0"
