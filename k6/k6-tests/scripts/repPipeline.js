import http from "k6/http";
import { check, group, sleep } from "k6";
import { Counter, Rate, Trend } from "k6/metrics";

export default function() {
  group ("grpEcho", function() {
    let res0 = http.get("http://rep-crispml-service:8000/echo?msg=helloworld", { timeout: 180000 });
    let checkRes0 = check(res0, {"status is 200": (r) => r.status === 200});
    //checkFailureRate.add(!checkRes0);
    let res9 = http.get("http://rep-crispml-service:8000/sleep?dur=1", { timeout: 180000 });
    let checkRes9 = check(res9, {"status is 200": (r) => r.status === 200});
    //checkFailureRate.add(!checkRes0);
  });
  group ("grpIngest", function() {
    //let res1 = http.get("http://rep-crispml-service:8000/crispml/repDataIngest", { timeout: 180000 });
    //let checkRes1 = check(res1, {"status is 200": (r) => r.status === 200});
    //checkFailureRate.add(!checkRes1);
    let res2 = http.get("http://rep-crispml-service:8000/crispml/repDataPrepare", { timeout: 180000 });
    let checkRes2 = check(res2, {"status is 200": (r) => r.status === 200});
    //checkFailureRate.add(!checkRes2);
    let res3 = http.get("http://rep-crispml-service:8000/crispml/repDataCurate", { timeout: 180000 });
    let checkRes3 = check(res3, {"status is 200": (r) => r.status === 200});
    //checkFailureRate.add(!checkRes3);
  });
  group ("grpTrain", function() {
    let res4 = http.get("http://rep-crispml-service:8000/crispml/repModelTrain", { timeout: 180000 });
    let checkRes4 = check(res4, {"status is 200": (r) => r.status === 200});
    //checkFailureRate.add(!checkRes4);
  });
};
